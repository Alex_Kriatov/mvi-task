package com.example.mvitask

import com.example.mvitask.api.Pagination
import com.example.mvitask.api.Quote
import com.example.mvitask.api.QuoteResponse
import com.example.mvitask.api.QuotesApi
import com.example.mvitask.core.DependencyProvider
import com.example.mvitask.repository.ItemRepositoryImpl
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class RepositoryUnitTest {
    companion object {
        private val mockQuotes = listOf(
            Quote(0, "0", "0", "0", "0"),
            Quote(1, "1", "1", "1", "1"),
            Quote(2, "2", "2", "2", "2")
        )
    }

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Before
    fun init() {
        val provider = object : DependencyProvider {
            override fun provideApi() = object : QuotesApi {
                override suspend fun getAllQuotes(): QuoteResponse {
                    return QuoteResponse(
                        mockQuotes,
                        "",
                        Pagination(
                            0,
                            0,
                            1
                        ),
                        200,
                        3
                    )
                }
            }
        }
        ItemRepositoryImpl.initRepository(provider)
    }

    @Test
    fun testGetAllItems() = coroutinesTestRule.runBlockingTest {
        val result = ItemRepositoryImpl.getAllItems()
        assertEquals(mockQuotes, result)
    }

    @Test
    fun testItemById() = coroutinesTestRule.runBlockingTest {
        ItemRepositoryImpl.getAllItems()
        val result = ItemRepositoryImpl.getItemById(mockQuotes.first()._id)
        assertEquals(mockQuotes.first(), result)
    }
}