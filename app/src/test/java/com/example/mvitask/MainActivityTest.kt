package com.example.mvitask

import android.widget.TextView
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView
import androidx.test.platform.app.InstrumentationRegistry
import com.example.mvitask.api.Pagination
import com.example.mvitask.api.Quote
import com.example.mvitask.api.QuoteResponse
import com.example.mvitask.api.QuotesApi
import com.example.mvitask.core.DependencyProvider
import com.example.mvitask.repository.ItemRepositoryImpl
import com.example.mvitask.ui.description.DescriptionFragment
import com.example.mvitask.ui.list.ListFragment
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.shadows.ShadowLooper.runUiThreadTasksIncludingDelayedTasks


@RunWith(RobolectricTestRunner::class)
class MainActivityTest {

    companion object {
        private val mockQuotes = listOf(
            Quote(0, "0", "0", "0", "0"),
            Quote(1, "1", "1", "1", "1"),
            Quote(2, "2", "2", "2", "2")
        )
    }

   private fun initProvider() {
        val provider = object : DependencyProvider {
            override fun provideApi() = object : QuotesApi {
                override suspend fun getAllQuotes(): QuoteResponse {
                    return QuoteResponse(
                        mockQuotes,
                        "",
                        Pagination(
                            0,
                            0,
                            1
                        ),
                        200,
                        3
                    )
                }
            }
        }
        ItemRepositoryImpl.initRepository(provider)
    }

    @Test
    fun assertListFragmentShown() {
        val mainActivity = Robolectric.buildActivity(MainActivity::class.java).setup().get()
        assertEquals(
            ListFragment::class.java.simpleName,
            mainActivity.supportFragmentManager.fragments.first().javaClass.simpleName
        )
    }

    @Test
    fun assertDetailsFragmentShown() {
        initProvider()
        val mainActivity = Robolectric.buildActivity(MainActivity::class.java).setup().get()
        mainActivity.findViewById<RecyclerView>(R.id.recycler).children.first().performClick()
        runUiThreadTasksIncludingDelayedTasks()
        assertEquals(
            DescriptionFragment::class.java.simpleName,
            mainActivity.supportFragmentManager.fragments.first().javaClass.simpleName
        )
        runUiThreadTasksIncludingDelayedTasks()
        val descriptionFragment = mainActivity.supportFragmentManager.fragments.first()
        val title = descriptionFragment.view?.findViewById<TextView>(R.id.title)
        val description = descriptionFragment.view?.findViewById<TextView>(R.id.editDescription)
        assertEquals(mockQuotes.first().quoteText, title?.text)
        assertEquals(mockQuotes.first().quoteAuthor, description?.text)
    }
}