package com.example.mvitask

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.mvitask.core.navigation.NavigationIntent
import com.example.mvitask.core.navigation.NavigationState
import com.example.mvitask.core.navigation.NavigationViewModel
import com.example.mvitask.ui.description.DescriptionFragment
import com.example.mvitask.ui.list.ListFragment

class MainActivity : AppCompatActivity() {

    private val navigation: NavigationViewModel by viewModels()
    private val rootId = R.id.container

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigation.state.observeForever(::updateState)
        navigation.navigate(NavigationIntent.List)
    }

    override fun onBackPressed() {
        navigation.navigateBack()
    }

    private fun updateState(state: NavigationState) {
        Log.d("qqq", "new state $state")
        val fragment = when (state) {
            is NavigationState.Finish -> {
                finish()
                null
            }
            is NavigationState.ItemsList -> ListFragment()
            is NavigationState.ItemDescription -> DescriptionFragment.newInstance(state.id)
        }
        fragment?.let {
            with(supportFragmentManager){
                if (isDestroyed.not() and isFinishing.not())
                    beginTransaction().replace(rootId, it).commit()
            }
        }
    }
}