package com.example.mvitask.repository

import com.example.mvitask.api.Quote
import com.example.mvitask.core.DependencyProvider

object ItemRepositoryImpl : ItemRepository {

    private var provider: DependencyProvider? = null
    private var cache: List<Quote>? = null

    override suspend fun getAllItems(): List<Quote>? =
        try {
            if (cache.isNullOrEmpty())
                cache = provider?.provideApi()?.getAllQuotes()?.data
            cache
        } catch (e: Throwable) {
            e.printStackTrace()
            null
        }

    override suspend fun getItemById(id: String) = cache?.find { it._id == id }

    fun initRepository(provider: DependencyProvider) {
        this.provider = provider
    }
}