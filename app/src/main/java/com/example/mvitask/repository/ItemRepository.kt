package com.example.mvitask.repository

import com.example.mvitask.api.Quote

interface ItemRepository {
    suspend fun getAllItems(): List<Quote>?
    suspend fun getItemById(id: String): Quote?
}