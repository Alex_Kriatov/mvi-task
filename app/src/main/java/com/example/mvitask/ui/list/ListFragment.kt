package com.example.mvitask.ui.list

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mvitask.R
import com.example.mvitask.api.Quote
import com.example.mvitask.core.BaseFragment
import com.example.mvitask.core.navigation.NavigationIntent
import com.example.mvitask.model.State

class ListFragment : BaseFragment() {

    private val viewModel: ListViewModel by viewModels()
    private val adapter by lazy { ItemAdapter(::onItemClicked) }

    override val resId: Int
        get() = R.layout.fragment_items_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(view.findViewById<RecyclerView>(R.id.recycler)) {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            adapter = this@ListFragment.adapter
        }
        viewModel.state.observe(requireActivity(), ::handleState)
        viewModel.loadItems()
    }

    @Suppress("UNCHECKED_CAST")
    override fun handleState(state: State) {
        if (state is State.Data<*>) {
            (state.data as? List<Quote>)?.let { adapter.submitList(it) }
        }
        view?.findViewById<ProgressBar>(R.id.progress)?.visibility = if (state is State.Loading) {
            View.VISIBLE
        } else View.GONE
    }

    private fun onItemClicked(item: Quote) {
        navigation.navigate(NavigationIntent.Description(item._id))
    }
}