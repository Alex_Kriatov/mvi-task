package com.example.mvitask.ui.description

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mvitask.model.State
import com.example.mvitask.repository.ItemRepositoryImpl
import kotlinx.coroutines.launch

class DescriptionViewModel : ViewModel() {

    private val _state = MutableLiveData<State>(State.Idle)
    val state: LiveData<State>
        get() = _state

    fun loadItem(id: String) {
        viewModelScope.launch {
            val item = ItemRepositoryImpl.getItemById(id)
            _state.postValue(
                if (item == null) State.Error("Can't find element")
                else State.Data(item)
            )
        }
    }
}