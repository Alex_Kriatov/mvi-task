package com.example.mvitask.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.mvitask.R
import com.example.mvitask.api.Quote

class ItemAdapter(val onItemClick: (item: Quote) -> Unit) : ListAdapter<Quote, ItemAdapter.ItemViewHolder>(object : DiffUtil.ItemCallback<Quote>(){
    override fun areItemsTheSame(oldItem: Quote, newItem: Quote): Boolean {
       return oldItem._id == newItem._id
    }

    override fun areContentsTheSame(oldItem: Quote, newItem: Quote): Boolean {
        return oldItem == newItem
    }

}) {

    inner class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view){
        fun bind(item: Quote){
            view.findViewById<TextView>(R.id.itemTitle).text = item.quoteText
            view.findViewById<TextView>(R.id.itemDescription).text = item.quoteAuthor
            view.setOnClickListener { onItemClick.invoke(item) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_items_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}