package com.example.mvitask.ui.description

import android.os.Bundle
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import com.example.mvitask.R
import com.example.mvitask.api.Quote
import com.example.mvitask.core.BaseFragment
import com.example.mvitask.model.State

class DescriptionFragment : BaseFragment() {

    override val resId: Int
        get() = R.layout.fragment_item_description

    private val viewModel: DescriptionViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.state.observe(requireActivity(), ::handleState)
        viewModel.loadItem(arguments?.get(KEY_ITEM_ID) as String)
    }


    override fun handleState(state: State) {
        if (state is State.Data<*>) {
            val title = view?.findViewById<TextView>(R.id.title)
            val description = view?.findViewById<TextView>(R.id.editDescription)
            title?.text = (state.data as Quote).quoteText
            description?.text = state.data.quoteAuthor
        }
    }

    companion object {
        private const val KEY_ITEM_ID = "item_id"
        fun newInstance(id: String): DescriptionFragment {
            return DescriptionFragment().apply { arguments = bundleOf(Pair(KEY_ITEM_ID, id)) }
        }
    }
}