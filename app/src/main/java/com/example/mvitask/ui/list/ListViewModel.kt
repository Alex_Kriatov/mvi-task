package com.example.mvitask.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mvitask.model.State
import com.example.mvitask.repository.ItemRepositoryImpl
import kotlinx.coroutines.launch

class ListViewModel : ViewModel() {

    private val _state = MutableLiveData<State>(State.Idle)
    val state: LiveData<State>
        get() = _state

    fun loadItems() {
        viewModelScope.launch {
            _state.value = State.Loading
            val items = ItemRepositoryImpl.getAllItems()
            if (items.isNullOrEmpty())
            _state.postValue(State.Error("Error"))
            else _state.postValue(State.Data(items))
        }
    }
}