package com.example.mvitask.api

data class Quote(
    val __v: Int,
    val _id: String,
    val quoteAuthor: String,
    val quoteGenre: String,
    val quoteText: String
)