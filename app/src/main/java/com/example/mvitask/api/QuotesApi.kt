package com.example.mvitask.api

import retrofit2.http.GET

interface QuotesApi {
    @GET("/api/v3/quotes")
    suspend fun getAllQuotes() : QuoteResponse
}