package com.example.mvitask.api

data class Pagination(
    val currentPage: Int,
    val nextPage: Int,
    val totalPages: Int
)