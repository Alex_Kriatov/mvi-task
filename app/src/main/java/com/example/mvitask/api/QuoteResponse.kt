package com.example.mvitask.api

data class QuoteResponse(
    val `data`: List<Quote>,
    val message: String,
    val pagination: Pagination,
    val statusCode: Int,
    val totalQuotes: Int
)