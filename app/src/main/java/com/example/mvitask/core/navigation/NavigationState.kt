package com.example.mvitask.core.navigation

sealed class NavigationState {
    object ItemsList : NavigationState()
    object Finish: NavigationState()
    data class ItemDescription(val id: String): NavigationState()
}