package com.example.mvitask.core

import android.app.Application
import com.example.mvitask.api.QuotesApi
import com.example.mvitask.repository.ItemRepositoryImpl
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class App : Application(), DependencyProvider {

    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl("https://quote-garden.herokuapp.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private val api: QuotesApi by lazy {
        retrofit.create(QuotesApi::class.java)
    }

    override fun onCreate() {
        super.onCreate()
        ItemRepositoryImpl.initRepository(this)
    }

    override fun provideApi(): QuotesApi = api
}