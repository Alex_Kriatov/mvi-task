package com.example.mvitask.core.navigation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.*

class NavigationViewModel : ViewModel() {

    private val navigationStack: Deque<NavigationState> = LinkedList()
    private val _state = MutableLiveData<NavigationState>()
    val state: LiveData<NavigationState>
        get() = _state

    fun navigateBack() {
        navigationStack.removeLast()
        setNewState(navigationStack.peek() ?: NavigationState.Finish)
    }

    fun navigate(navigationIntent: NavigationIntent) {
        handleIntent(navigationIntent)
    }

    fun finish() {
        handleIntent(NavigationIntent.Finish)
    }

    private fun handleIntent(navigationIntent: NavigationIntent) {
        Log.d("qqq", "handle intent")
        when (navigationIntent) {
            is NavigationIntent.Finish -> {
                navigationStack.clear()
                NavigationState.Finish
            }
            is NavigationIntent.List -> NavigationState.ItemsList
            is NavigationIntent.Description -> NavigationState.ItemDescription(navigationIntent.id)
        }.also { setNewState(it) }
    }

    private fun setNewState(state: NavigationState) {
        if (state != navigationStack.peek()) navigationStack.add(state)
        _state.value = state
    }
}