package com.example.mvitask.core

import com.example.mvitask.api.QuotesApi

interface DependencyProvider{
    fun provideApi():QuotesApi
}