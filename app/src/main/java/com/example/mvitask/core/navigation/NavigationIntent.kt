package com.example.mvitask.core.navigation

sealed class NavigationIntent {
    object Finish: NavigationIntent()
    object List: NavigationIntent()
    data class Description(val id: String): NavigationIntent()
}