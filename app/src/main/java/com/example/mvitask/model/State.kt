package com.example.mvitask.model

sealed class State {
    object Loading : State()
    object Idle : State()
    data class Data<T>(val data: T) : State()
    data class Error(val msg: String?) : State()
}